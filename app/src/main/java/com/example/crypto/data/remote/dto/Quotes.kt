package com.example.crypto.data.remote.dto


import com.google.gson.annotations.SerializedName

data class Quotes(
    @SerializedName("USD")
    val usd: Currency
)