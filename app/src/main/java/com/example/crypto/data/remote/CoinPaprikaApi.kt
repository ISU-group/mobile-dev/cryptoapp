package com.example.crypto.data.remote

import com.example.crypto.data.remote.dto.CoinDto
import retrofit2.http.GET
import retrofit2.http.Path

interface CoinPaprikaApi {
    @GET("/v1/tickers")
    suspend fun getCoins(): List<CoinDto>

    @GET("/v1/tickers/{coinId}")
    suspend fun getCoinById(@Path("coinId") coinId: String): CoinDto
}