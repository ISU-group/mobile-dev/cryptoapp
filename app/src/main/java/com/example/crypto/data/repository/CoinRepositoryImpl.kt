package com.example.crypto.data.repository

import com.example.crypto.data.remote.CoinPaprikaApi
import com.example.crypto.data.remote.dto.CoinDto
import com.example.crypto.domain.repository.CoinRepository
import javax.inject.Inject

class CoinRepositoryImpl @Inject constructor(
    private val api: CoinPaprikaApi
) : CoinRepository {
    override suspend fun getCoins(): List<CoinDto> = api.getCoins()

    override suspend fun getCoinById(coinId: String): CoinDto = api.getCoinById(coinId)
}