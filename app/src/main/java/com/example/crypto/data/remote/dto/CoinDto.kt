package com.example.crypto.data.remote.dto


import com.example.crypto.domain.model.Coin
import com.google.gson.annotations.SerializedName

data class CoinDto(
    @SerializedName("beta_value")
    val betaValue: Double,
    @SerializedName("circulating_supply")
    val circulatingSupply: Long,
    @SerializedName("first_data_at")
    val firstDataAt: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("last_updated")
    val lastUpdated: String,
    @SerializedName("max_supply")
    val maxSupply: Long,
    @SerializedName("name")
    val name: String,
    @SerializedName("quotes")
    val quotes: Quotes,
    @SerializedName("rank")
    val rank: Int,
    @SerializedName("symbol")
    val symbol: String,
    @SerializedName("total_supply")
    val totalSupply: Long
)

fun CoinDto.toCoin(): Coin
    = Coin(
        firstDataAt = firstDataAt,
        id = id,
        lastUpdated = lastUpdated,
        name = name,
        rank = rank,
        symbol = symbol,
        price = quotes.usd.price
    )