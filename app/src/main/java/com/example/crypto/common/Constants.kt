package com.example.crypto.common

object Constants {
    const val BASE_URL = "https://api.coinpaprika.com"

    const val COIN_ID_PARAM = "coinId"
}