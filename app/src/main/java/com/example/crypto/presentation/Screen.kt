package com.example.crypto.presentation

sealed class Screen (val route: String) {
    object CoinListScreen: Screen("coin_list_screen")
    object CoinScreen: Screen("coin_screen")
}