package com.example.crypto.presentation

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.crypto.common.Constants
import com.example.crypto.presentation.coin_list.CoinListScreen
import com.example.crypto.presentation.theme.CryptoTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    private val coroutineScope = CoroutineScope(Dispatchers.IO)
    private lateinit var job: Job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        job = getCoroutineJob()


//        retrofit?.let {
//            val service = it.create(TestService::class.java)
//            val call = service.getData(
//                fsym = "BTC",
//                tsyms = "USD"
//            )
//
//            call.enqueue(object : Callback<Crypto> {
//                override fun onResponse(call: Call<Crypto>, response: Response<Crypto>) {
//                    if (response.code() == 200) {
//                        Log.d("mytag", "Response message: ${response.body()}")
//                        response.body()?.let {
//                            Log.d("mytag", "Response: $it")
//                        }
//                    } else {
//                        Log.d("mytag", "Response code: ${response.message()}")
//                    }
//                }
//
//                override fun onFailure(call: Call<Crypto>, t: Throwable) {
//                    Log.d("mytag", "Request FAILED!")
//                }
//            })
//
//        }


        setContent {
            CryptoTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()
                    NavHost(
                        navController = navController,
                        startDestination = Screen.CoinListScreen.route
                    ) {
                        composable(route = Screen.CoinListScreen.route) {
                            CoinListScreen(navController = navController)
                        }

                        composable(route = "${Screen.CoinScreen.route}/{coinId}") {
//                            CoinScreen(navController = navController)
                            Text(
                                text = "Coin Item"
                            )
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()

        if (job.isCompleted) {
            job = getCoroutineJob()
            Log.d("mytag", "Coroutine started: ${job.start()}")
        }
    }

    override fun onPause() {
        super.onPause()

        if (job.isActive) {

            job.cancel()
            Log.d("mytag", "Coroutine canceled")
        }
    }

    private fun getCoroutineJob(): Job
        = coroutineScope.launch {

        }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}
//
//interface TestService {
//    // fsym=BTC&tsyms=USD,JPY,EUR
//    @GET("data/price?")
//    fun getData(
//        @Query("fsym") fsym: String,
//        @Query("tsyms") tsyms: String
//    ): Call<Crypto>
//}