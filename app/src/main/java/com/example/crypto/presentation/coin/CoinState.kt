package com.example.crypto.presentation.coin

import com.example.crypto.domain.model.Coin

data class CoinState(
    val isLoading: Boolean = false,
    val coin: Coin? = null,
    val error: String = ""
)
