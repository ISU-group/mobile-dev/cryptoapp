package com.example.crypto.presentation.coin

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.crypto.common.Constants
import com.example.crypto.common.Resource
import com.example.crypto.domain.use_case.get_coin.GetCoin
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class CoinViewModel @Inject constructor(
    private val getCoinUseCase: GetCoin,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _state = mutableStateOf(CoinState())
    val state = _state

    init {
        savedStateHandle.get<String>(Constants.COIN_ID_PARAM)?.let { coinId ->
            getCoin(coinId)
        }
    }

    private fun getCoin(coinId: String) = getCoinUseCase(coinId).onEach {  resource ->
        _state.value = when (resource) {
            is Resource.Success -> CoinState(coin = resource.data)
            is Resource.Error -> CoinState(error = resource.message ?: "An unexpected error occurred")
            is Resource.Loading -> CoinState(isLoading = true)
        }
    }.launchIn(viewModelScope)
}