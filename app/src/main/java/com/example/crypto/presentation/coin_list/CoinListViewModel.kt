package com.example.crypto.presentation.coin_list

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.crypto.common.Resource
import com.example.crypto.domain.use_case.get_coins.GetCoins
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class CoinListViewModel @Inject constructor(
    private val getCoinsUseCase: GetCoins
) : ViewModel() {

    private val _state = mutableStateOf<CoinListState>(CoinListState())
    val state = _state

    init {
        getCoins()
    }

    private fun getCoins() = getCoinsUseCase().onEach {  resource ->
        _state.value = when (resource) {
            is Resource.Success -> CoinListState(coins = resource.data ?: emptyList())
            is Resource.Error -> CoinListState(error = resource.message ?: "An unexpected error occurred")
            is Resource.Loading -> CoinListState(isLoading = true)
        }
    }.launchIn(viewModelScope)
}