package com.example.crypto.domain.model


data class Coin(
    val firstDataAt: String,
    val id: String,
    val lastUpdated: String,
    val name: String,
    val rank: Int,
    val symbol: String,
    val price: Double,
)
